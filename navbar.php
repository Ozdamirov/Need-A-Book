	<!-- **********************NAVBAR*************************************** -->
	<nav class="navbar navbar-expand-lg navbar-light">
		<h1 class="mr-auto ">Need <span style="color:#6EC8C7">a</span> Book</h1>
		<div class="navbar-brand" href="#haut"><img src="images/bookicon.png" alt="logo" style="margin-left: 20px;margin-top: 10px"> <span style="color:#6EC8c7">~</span><span style="color: #ffffff; font-size: 0.9em"> Make your books come true</span> <span style="color:#6EC8C7">~<span></div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="#haut">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="about.html">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.html">Contact me</a>
					</li>
				</ul>
			</div>
     </nav>
