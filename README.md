Copyright - Mansour Ozdamirov 2019 - Need a Book

Project : "Need a Book".

Need a Book will be a web search application for books according to the criteria of the user.
Users will be mostly people who have a lack of ideas on the selection of their next book, therefore, we'll remedy this problem by offering a multitude of ideas of books to read
Developed as part of the 2nd year of BTS SIO, the website have been developped in PHP.

URL :
http://www.bts-malraux72.net/~m.ozdamirov/PPE4_NeedABook/Need-A-Book/index.php
Author :
Mansour Ozdamirov