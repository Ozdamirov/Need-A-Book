<?php
include("connexion.php");
?>

<!doctype html>
<html lang="fr">
<?php
include("head.php");
?>
<body id="haut">
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<?php
include("navbar.php");
?>
        <div class="row align-items-center">
            <!-- 1ère colonne -->
            <div class="col-md-12 row1">
                <h2>Looking for a book ?</h2>
                <p>You're at the right place</p>
            </div>
            <!-- 2nd colonne -->
            <div class="col-md-12">
                <h3>Tell me more</h3>
            </div>
<?php
include("formulaire.php");
?>
    </header>
<?php
include("footer.php");
?>
</body>
</html>
